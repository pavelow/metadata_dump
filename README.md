# Audio Metadata Dumper
Dumps audio metadata to json file

## Setup
Depends on [tinytag](https://pypi.org/project/tinytag/) for metadata parsing.
- `pip install -r requirements.txt` will install the dependency.

## Usage
```
usage: mdump.py [-h] [-f FOLDER] [-o OUTPUT]

Metadata Dumper

optional arguments:
  -h, --help            show this help message and exit
  -f FOLDER, --folder FOLDER
                        Folder to dump metadata from
  -o OUTPUT, --output OUTPUT
```

### Example
`python3 mdump.py -f /path/to/audio/files -o /path/to/output/file.json`
