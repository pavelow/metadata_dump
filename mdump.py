#! /usr/bin/env python3

import  os, argparse, sys, json
from tinytag import TinyTag


def dumpFolder(folder):
	data = {}
	for file in os.listdir(folder):
		data[file] = dumpFile(folder+os.path.sep+file)
	return data

def dumpDict(tag):
	tagData = {}
	tagData['album'] = tag.album
	tagData['albumartist'] = tag.albumartist
	tagData['artist'] = tag.artist
	tagData['comment'] = tag.comment
	tagData['composer'] = tag.composer
	tagData['duration'] = tag.duration
	tagData['genre'] = tag.genre
	tagData['title'] = tag.title
	tagData['year'] = tag.year
	return tagData

def dumpFile(fileName):
	if os.path.isdir(fileName):
		return dumpFolder(fileName) # Recurse into subfolder
	else:
		data = {}
		if TinyTag.is_supported(fileName):
			data = dumpDict(TinyTag.get(fileName))
		return data

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Metadata Dumper")
	parser.add_argument('-f', '--folder', action="store", help="Folder to dump metadata from")
	parser.add_argument('-o', '--output', action="store", help="Destination file")
	args = parser.parse_args()

	if not args.output:
		print("No Destination file!")
		sys.exit(-1)
	if not args.folder:
		print("No Folder to dump!")
		sys.exit(-1)

	data = dumpFolder(args.folder)
	with open(args.output,'w') as f:
		f.write(json.dumps(data))
	print("Written to \"" + args.output +"\"")
